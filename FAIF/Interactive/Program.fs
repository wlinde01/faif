﻿open Microsoft.FSharp.NativeInterop
//open System.IO
open Network
open Teacher.Network
open Utils.IO
open Utils.ActivationFunctions
open Teacher.TrainingFunctions
open System


// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

let rec readlines () = seq {
    let line = Console.ReadLine()
    if line <> null then
        yield line
        yield! readlines ()
}

[<EntryPoint>]
let main argv = 
    // What sort of stuff should the interactive interface provide?
    // Create NN
    // Open NN from file location 
    // Open training data and train network until ....
    // Etc Etc... 

    printfn "Output %A" argv
    let x = System.Console.ReadLine 

    let csvReader = CsvReader("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData : List<List<float>> = csvReader.openCsv ("xor") 
    
    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 2)

    let hiddenLayer = Layer.Spawn 2 2 LogisticSigmoidFunction HiddenLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"
    let answer = trainedXORNetwork.Activate [0.0;0.0]
    printf "Answer:%f" answer.[0]

    0 // return an integer exit code
