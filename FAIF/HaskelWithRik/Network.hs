
module Network where

    import Data.List

    foo :: String
    foo = "bar"

    type I = Integer
    type W = Integer
    type P = Integer
    type S = Integer
    type D = Integer
    type T = Integer

    ps :: I -> [W] -> [P]
    ps i ws = map (*i) ws

    pss :: [I] -> [[W]] -> [[P]]
    pss is wss = (ps (head is) (head wss)):(pss (tail is) (tail wss))

    qss :: [[P]] -> [P]
    qss pss = qss' (transpose pss)
    qss' [] = []
    qss' pss = (foldl (+) 0 (head pss)):(qss' (tail pss))

    ds :: [S] -> [T] -> [D]
    ds [] _ = []
    ds ss ts = ((head ss) - (head ts)):(ds (tail ss) (tail ts))

    dss :: [I] -> [[[W]]] -> [[T]] -> [[D]]
    dss [] _ _ = []
    dss is wsss tss =
        let
            qss'' = qss (pss is (head wsss))
            ds' = ds (qss'') (head tss)
        in
        (ds'):
        (dss is (tail wsss) (tail tss))

--     ss :: [P] -> S; s ps = foldl (+) 0 ps
--     d :: S -> T -> D; d s t = s - t
--     ds :: [S] -> [T] -> [D]

--     ds [] [] = []
--     ds ss ts = (d (head ss) (head ts)):(ds (tail ss) (tail ts))


--     working is wss tss

--     ps :: [I] -> [W] -> [P]
--     ps is ws = map p (zip is ws)
-- ew :: (Int, Int) -> Int
-- ew (e, w) = e * w
--
-- ews :: [Int]
--
-- g ::
