﻿module Tests.Perceptron
open Xunit
open FsUnit
open Neuron
open Utils.ActivationFunctions
open Utils.Functions
open Teacher.TrainingFunctions
open Teacher.Perceptron
//open FsUnit.Xunit


// ToDo: Where do these two live? 
// ToDo: Convert from vanila xunit to fsunit 
let threshold = 0.2 // ToDo Thresholds are perceptron specific 
let learningRate = 0.1


[<Fact>] 
let ``Creates``() = // Perceptron Creates Itself Correctly
    let inputs = [1.0;2.0;3.0]
    let weights = [1.0;2.0;3.0]
    let actualPerceptron = Neuron.create weights threshold
        
  //  actualPerceptron.SumByWeights (inputs, threshold) |> should equal 13.8 // ToDo This is not part of creation
    actualPerceptron.Activate (inputs, StepFunction) |> should equal 1.0 // send in activation funcion, biass?
    actualPerceptron.Activate (inputs, StepFunction) |> should not' (equal 0.0) // send in activation funcion, biass?
    

//[<Fact>]
//let ``Sums by Weights``() = // Perceptron Sums all Weights Correctly
//    let perceptron = { Weights = [0.2; 0.0]}
//    let inputs = [1.0;1.0]
//    perceptron.SumByWeights (inputs, StepFunction, threshold) |> should equal 0.0
    

[<Fact>]
let ``Activates``() = 
    let perceptron = Neuron.create [0.2; 0.0] threshold
    let inputs = [1.0;1.0]

    perceptron.Activate(inputs, StepFunction) |> should equal 1.0
  

// ToDo : Describe test better .. 
[<Fact>]
let ``Integration (Creation; Weights; Sum by weights; Activation; Error; Delta; Training )``() =

    let inputs = [1.0;0.0]
    let weights = [0.3; -0.1]
    let actualPerceptron = Neuron.create weights threshold 
    
    let desiredOutput = 0.0
    let actualOutput = actualPerceptron.Activate(inputs, StepFunction)
    
    // Test weights
    Assert.Equal(0.3, actualPerceptron.Weights.Item(0))    
    Assert.Equal(-0.1, actualPerceptron.Weights.Item(1))
    
//    // Test Sum by Weights
//    let sumByWeight = actualPerceptron.SumByWeights (inputs, StepFunction, threshold)
//    Assert.Equal(0.1, sumByWeight, 4)

    // Test Activation
    Assert.Equal(1.0, actualOutput)

    // Test Error
    let error = Error(desiredOutput, actualOutput)
    Assert.Equal(-1.0, error)
    
    // Test Delta
    let delta = Delta(inputs.Item(0), desiredOutput, actualOutput, learningRate)
    Assert.Equal (-0.1, delta) // <<-- check this, no confidence in tests or implimentation

    //Test Weight Adjustment
    Assert.Equal(0.2, AdjustPerceptronWeight(0.3, 1.0, 0.0, 1.0, learningRate), 4)

    // Test trained perceptron's new weights
    let perceptron = Neuron.create weights threshold
    let actualTrainedPerceptron = TrainPerceptron(perceptron, inputs, desiredOutput, threshold, learningRate)
    Assert.Equal(0.2, actualTrainedPerceptron.Weights.Item(0),4)    
    Assert.Equal(-0.1, actualTrainedPerceptron.Weights.Item(1),4)    

    // Test trained perceptron's new weights with alternative starting inputs and weights
    let inputs, weights = [1.0;1.0], [0.2; -0.1]
    let perceptron = Neuron.create weights threshold
    let actualTrainedPerceptron' = TrainPerceptron(perceptron, inputs, 1.0, threshold, learningRate)
    Assert.Equal(0.3, actualTrainedPerceptron'.Weights.Item(0),4)    
    Assert.Equal(0.0, actualTrainedPerceptron'.Weights.Item(1),4)    


[<Fact>]
let TestPerceptronWeightAdjustmentsDuringTraining() =

    // Test when NO adjustment should happen
    let inputs = [1.0;1.0]
    let weights = [0.2; 0.0]
    let actualPerceptron = Neuron.create weights threshold
    
    let desiredOutput = 1.0
    //let actualOutput = actualPerceptron.Activate
    let perceptron = Neuron.create weights threshold
    let trainedWeights = TrainPerceptron(perceptron, inputs, desiredOutput, threshold, learningRate).Weights
        
    Assert.Equal(weights.Item(1), trainedWeights.Item(1),4)
    Assert.Equal(weights.Item(0), trainedWeights.Item(0),4)

    // Test when adjustment SHOULD happen
    let inputs = [1.0;0.0]
    let weights = [0.3; 0.0]
    let actualPerceptron = (Neuron.create weights)
    
    let desiredOutput = 0.0
    let desiredWeights = [0.2;0.0]
    // let actualOutput = actualPerceptron.Activate
    let perceptron = Neuron.create weights threshold
    let trainedWeights = TrainPerceptron(perceptron, inputs, desiredOutput, threshold, learningRate).Weights

    Assert.Equal(desiredWeights.Item(1), trainedWeights.Item(1),4)
    Assert.Equal(desiredWeights.Item(0), trainedWeights.Item(0),4)

    
[<Fact>]
let ``Learns to do logical AND``() =
    let inputs = [[0.0;0.0];[0.0;1.0];[1.0;0.0];[1.0;1.0]]
    let desiredOutputs = [0.0;0.0;0.0;1.0]
    let initialWeights = [0.3; -0.1]
    let trainedPerceptron =  trainPerceptronWithEpocUntil(inputs, desiredOutputs, initialWeights, 3, threshold, learningRate, StepFunction)

    // Perceptron should be trained by now, test it's outputs / activations
    trainedPerceptron.Activate([0.0;0.0], StepFunction) |> should equal 0.0
    trainedPerceptron.Activate([0.0;1.0], StepFunction) |> should equal 0.0
    trainedPerceptron.Activate([1.0;0.0], StepFunction) |> should equal 0.0
    trainedPerceptron.Activate([1.0;1.0], StepFunction) |> should equal 1.0
    
[<Fact>]
let ``Learns to do logical OR``() =
    let inputs = [[0.0;0.0];[0.0;1.0];[1.0;0.0];[1.0;1.0]]
    let desiredOutputs = [0.0;1.0;1.0;1.0]
    let initialWeights = [0.3; -0.1]    
    let initialPerceptron = Neuron.create initialWeights
  
    let trainedPerceptron =  trainPerceptronWithEpocUntil(inputs, desiredOutputs, initialWeights, 3, threshold, learningRate, StepFunction)

    // Perceptron should be trained by now, test it's outputs / activations
    trainedPerceptron.Activate([0.0;0.0], StepFunction) |> should equal 0.0
    trainedPerceptron.Activate([0.0;1.0], StepFunction) |> should equal 1.0
    trainedPerceptron.Activate([1.0;0.0], StepFunction) |> should equal 1.0
    trainedPerceptron.Activate([1.0;1.0], StepFunction) |> should equal 1.0

[<Fact>]
let ``Neuron spawns random weights and threshold based on Haykin, 1999``() =    

    // tmp test neuron spawns randomly
    //let neuronWith2Weights = Neuron.spawn 2
    let neuronWith2Weights = Neuron.Spawn 2
    
    let lowerHaykin x = -2.4/float(x)
    let upperHaykin x = 2.4/float(x)

    neuronWith2Weights.Threshold 
    |> should be (greaterThan (lowerHaykin(2) ))
    neuronWith2Weights.Threshold 
    |> should be (lessThan (upperHaykin(2) ))

    neuronWith2Weights.Weights.[0]
    |> should be (greaterThan (lowerHaykin(2) ))
    neuronWith2Weights.Weights.[0]
    |> should be (lessThan (upperHaykin(2) ))    

    neuronWith2Weights.Weights.[1]
    |> should be (greaterThan (lowerHaykin(2) ))
    neuronWith2Weights.Weights.[1]
    |> should be (lessThan (upperHaykin(2) ))    

    neuronWith2Weights.Weights.[0]
    |> should not' (equal (neuronWith2Weights.Weights.[1]))


[<Fact>]
let ``Test matrix transposition``() =
    let w1,w2 = ["y1w1";"y1w2";"y1w3"],["y2w1";"y2w2";"y2w3"]
    let weightMatrix = [w1;w2]
    let transposedWeights = weightMatrix |> transpose

    transposedWeights.[0] |> should equal ["y1w1";"y2w1"]
    transposedWeights.[1] |> should equal ["y1w2";"y2w2"]
    transposedWeights.[2] |> should equal ["y1w3";"y2w3"]

    let w1 = ["y1w1";"y1w2"]
    let weightMatrix = [w1]
    let transposedWeights = weightMatrix |> transpose

    transposedWeights.[0] |> should equal ["y1w1"]
    transposedWeights.[1] |> should equal ["y1w2"]

    0 |> should equal 0


[<Fact>]
let ``single layer network learns to do OR``() =
    let inputs = [[0.0;0.0];[0.0;1.0];[1.0;0.0];[1.0;1.0]]
    let desiredOutputs = [0.0;1.0;1.0;1.0]
    let initialWeights = [0.3; -0.1]    
  
    let trainedlayer =  trainLayerWithEpocUntil(inputs, desiredOutputs, initialWeights, 1, 3, threshold, learningRate, StepFunction)

    // Perceptron will now be trained
    trainedlayer.Activate([0.0;0.0]).[0] |> should equal 0.0
    trainedlayer.Activate([0.0;1.0]).[0] |> should equal 1.0 
    trainedlayer.Activate([1.0;0.0]).[0] |> should equal 1.0
    trainedlayer.Activate([1.0;1.0]).[0] |> should equal 1.0