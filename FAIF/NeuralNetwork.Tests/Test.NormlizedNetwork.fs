﻿module Test.NormalizedNetwork

open Xunit
open FsUnit
open Layer
open Network
open Utils.ActivationFunctions
open Teacher.TrainingFunctions
open Teacher.Network
//open FsUnit.Xunit // https://fsprojects.github.io/FsUnit/
open System
open Utils.IO
open Normalizer
open NormalizedNetwork


let getSimpleHyperBolicNetwork inputs outputs = 

    let hiddenLayer = Layer.Spawn inputs inputs HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn outputs hiddenLayer.Neurons.Length HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    { Layers = [hiddenLayer; outputLayer] }

[<Fact>]
let ``Test normalization persists, recalls and works along with trained network``() = 
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, normalizer = csvReader.openCsv (19,"cervicalcancer-citology") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let trainingDesiredOutPuts = getFirstHalf desiredOutputs

    let cancerNetwork = getSimpleHyperBolicNetwork 19 1
    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.02, cancerNetwork, 0.1)

    let networkWithNormalizer = {Network = trainedCancerNetwork; Normalizer = normalizer}

    let persister = Persiter()

    do persister.saveNormalizedNetwork (networkWithNormalizer, "cancerNetwork") |> ignore
    let retrievedNetwork : NormalizedNetwork =  persister.openNormalizedNetwork ("cancerNetwork")

    //do persister.saveNetwork (trainedCancerNetwork, "cancerNetwork") |> ignore
    //let retrievedNetwork =  persister.openNetwork ("cancerNetwork")

    let rawCsvReader = CsvReader("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let rawCsvData = rawCsvReader.openCsv ("cervicalcancer-citology-raw") 
    let rawInputs, rawDesiredOutputs = getTrainingDataAndDesiredOutput (rawCsvData, 19)
    
    let input = [25.0;2.0;17.0;0.0;0.0;0.0;0.0;1.0;0.08;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0]
    let result = retrievedNetwork.Activate input

    let normalizerFromPersistence = retrievedNetwork.getNormalizer
    let normalized = normalizerFromPersistence.GetStandardRow (input |> List.toArray) |> Seq.toList
    let fromCSVData = csvData.[csvData.Length-3]
    let resultFromCsv = csvData.[csvData.Length-3].[19]
    let resultTwo = trainedCancerNetwork.Activate normalized
    //resultTwo |> should equal 1.0 // -0.0014637016976304373d
    result |> should equal resultTwo // should be 1.0
        

    //printf "\nPre Network:\n%s" (SerializeToJson(trainedCancerNetwork))
    //printf "\nPersisted Network:\n%s" (SerializeToJson2(retrievedNetwork))

    let persNetworkOnly = retrievedNetwork.getNetwork

    // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    //let actualPrepersistedOutputs = [for i in 0..inputs.Length-1 do yield retrievedNetwork.Activate(rawInputs.[i])]
    let actualPrepersistedOutputs = [for i in 0..inputs.Length-1 do yield persNetworkOnly.Activate(inputs.[i])]
    let actualOutputs = [for i in 0..inputs.Length-1 do yield trainedCancerNetwork.Activate(inputs.[i])]

    printActualAndDesiredOutputs actualOutputs desiredOutputs
    printf "\n <<<<<<< >>>>>>>>>>> \n"
    printActualAndDesiredOutputs actualPrepersistedOutputs rawDesiredOutputs

    //for i in 0..actualOutputs.Length-1 do 
    //    for j in 0..actualOutputs.Head.Length-1 do
    //        Math.Round(actualOutputs.[i].[j],0) |> should equal testDesiredOutputs.[i].[j]




