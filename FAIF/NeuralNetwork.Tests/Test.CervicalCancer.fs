﻿module Test.CervicalCancer

open Xunit
open FsUnit
open Layer
open Network
open Utils.ActivationFunctions
open Teacher.TrainingFunctions
open Teacher.Network
//open FsUnit.Xunit // https://fsprojects.github.io/FsUnit/
open System
open Utils.IO
open Normalizer





[<Fact>]
let ``Cervical cancer test with all Diagnoses as seperate outputs catagorised``() = 
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19,"cervicalcancer-all-diagnoses-catagorised") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let testInputs = getSecondHalf inputs

    let trainingDesiredOutPuts = getFirstHalf desiredOutputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    let hiddenLayer1 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer2 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer3 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer4 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer5 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer6 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer7 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer8 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer9 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer10 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer11 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer12 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer13 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer14 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer15 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer16 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 



    let outputLayer = Layer.Spawn 16 hiddenLayer16.Neurons.Length LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum
    let  cancerNetwork = { Layers = [hiddenLayer1; hiddenLayer2; hiddenLayer3; hiddenLayer4; hiddenLayer5; hiddenLayer6; hiddenLayer7; hiddenLayer8; hiddenLayer9; hiddenLayer10; hiddenLayer11; hiddenLayer12; hiddenLayer13; hiddenLayer14; hiddenLayer15; hiddenLayer16; outputLayer] }
    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.0175, cancerNetwork, 189.0) 

    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

    //printActualAndDesiredOutputs actualOutputs testDesiredOutputs

    let nodeBasedAccuracy, outputLayerBasedAccuracy  = NetworkPerformanceMetrics (actualOutputs, testDesiredOutputs)
    outputLayerBasedAccuracy |> should be (greaterThan 85.0)



[<Fact>]
let ``Cervical cancer test with all Diagnoses as seperate outputs``() = 
    // Never gets error margin small enough, network performance % only looks >80% due to large amount of no cancer "outputs"
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19,"cervicalcancer-all-diagnoses") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let testInputs = getSecondHalf inputs

    let trainingDesiredOutPuts = getFirstHalf desiredOutputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    let hiddenLayer = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 

    let hiddenLayer4 = Layer.Spawn 8 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    
    let outputLayer = Layer.Spawn 8 hiddenLayer4.Neurons.Length HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction  adjustWeightsSimple

    let  cancerNetwork = { Layers = [hiddenLayer; hiddenLayer4; outputLayer] }

    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.0175, cancerNetwork, 110.0) 
   
       // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

  
    //printActualAndDesiredOutputs actualOutputs testDesiredOutputs
    let nodeBasedAccuracy, outputLayerBasedAccuracy  = NetworkPerformanceMetrics (actualOutputs, testDesiredOutputs)
    
    outputLayerBasedAccuracy |> should be (greaterThan 80.0)


[<Fact>]
let ``Cervical cancer test with stardardised data single output (all diagnoses reduced to single Yes / No answer) ``() = 
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19,"cervicalcancer-simple") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let testInputs = getSecondHalf inputs

    let trainingDesiredOutPuts = getFirstHalf desiredOutputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    let hiddenLayer = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer2 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer3 = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 2 hiddenLayer3.Neurons.Length LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  cancerNetwork = { Layers = [hiddenLayer; hiddenLayer2; hiddenLayer3; outputLayer] }

    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.012, cancerNetwork, 5.0) 

    // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

    printActualAndDesiredOutputs actualOutputs testDesiredOutputs 

    for i in 0..actualOutputs.Length-1 do 
        for j in 0..actualOutputs.Head.Length-1 do
            Math.Round(actualOutputs.[i].[j],0) |> should equal testDesiredOutputs.[i].[j]

[<Fact>]
let ``Cervical cancer test stardardised data Schiller diagnosis only``() = 
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19,"cervicalcancer-schiller") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let testInputs = getSecondHalf inputs

    let trainingDesiredOutPuts = getFirstHalf desiredOutputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    let hiddenLayer = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 hiddenLayer.Neurons.Length HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 

    let  cancerNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.02, cancerNetwork, 0.1) // Produces "good results in low ish amount of iterations"

    // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

    for i in 0..actualOutputs.Length-1 do 
        for j in 0..actualOutputs.Head.Length-1 do
            Math.Round(actualOutputs.[i].[j],0) |> should equal testDesiredOutputs.[i].[j]

[<Fact>]
let ``Cervical cancer test stardardised data Citology diagnosis only``() = 
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19,"cervicalcancer-citology") 

    //for i in 0..csvData.Length-1 do 
    //    printf "\n"
    //    for j in 0..csvData.Head.Length-1 do
    //        printf "%f," csvData.[i].[j] 



    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)



    let trainingInputs = getFirstHalf inputs
    let trainingDesiredOutPuts = getFirstHalf desiredOutputs


    let hiddenLayer = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 hiddenLayer.Neurons.Length LogisticSigmoidFunction  OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  cancerNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.015, cancerNetwork, 5.5) // Produces "good results in low ish amount of iterations"

    let testInputs = getSecondHalf inputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

    printActualAndDesiredOutputs testDesiredOutputs actualOutputs



    //for i in 0..actualOutputs.Length-1 do 
    //    for j in 0..actualOutputs.Head.Length-1 do
    //        Math.Round(actualOutputs.[i].[j],0) |> should equal testDesiredOutputs.[i].[j]





[<Fact>]
let ``Cervical cancer test with medium data i.e. two outputs``() = 
    // Gives close to correct results - miss fires, both (actual) outputs are "Active" for situations where desired outputs contain a single "positive" result. 
    // Rarely misfires on 0;0 outputs.
    let csvReader = CsvReaderWithNormilization("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData, _ = csvReader.openCsv (19, "cervicalcancer-medium") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

    let trainingInputs = getFirstHalf inputs
    let testInputs = getSecondHalf inputs

    let trainingDesiredOutPuts = getFirstHalf desiredOutputs
    let testDesiredOutputs = getSecondHalf desiredOutputs

    let hiddenLayer = Layer.Spawn 19 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer2 = Layer.Spawn 8 19 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer3 = Layer.Spawn 8 8 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer4 = Layer.Spawn 4 8 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    
    let outputLayer = Layer.Spawn 2 hiddenLayer4.Neurons.Length HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction  adjustWeightsSimple

    let  cancerNetwork = { Layers = [hiddenLayer; hiddenLayer2; hiddenLayer3; hiddenLayer4; outputLayer] }

    let trainedCancerNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (trainingInputs, trainingDesiredOutPuts, 0.0175, cancerNetwork, 20.0) // Produces "good results in low ish amount of iterations"
   
       // Run through entire section of "Test" data, try to work out how accurate network is on all input/output 
    let actualOutputs = [for i in 0..testInputs.Length-1 do yield trainedCancerNetwork.Activate(testInputs.[i])]

    //printActualAndDesiredOutputs actualOutputs testDesiredOutputs

    for i in 0..actualOutputs.Length-1 do 
        for j in 0..actualOutputs.Head.Length-1 do
            Math.Round(actualOutputs.[i].[j],0) |> should equal testDesiredOutputs.[i].[j]

    

