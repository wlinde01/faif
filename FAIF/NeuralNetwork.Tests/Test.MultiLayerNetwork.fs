﻿module Tests.MultiLayerNetwork
open Xunit
open FsUnit
open Neuron
open Layer
open Network
open Utils.ActivationFunctions
open Teacher.TrainingFunctions
open Teacher.Network
//open FsUnit.Xunit // https://fsprojects.github.io/FsUnit/
open System
open Utils.IO

// Using Theories
// http://fssnip.net/dE/title/Using-Theories-with-FSUnit-XUnit-and-NUnit

[<Fact>]
let ``Two layer logistic sigmoidal preset network with momentum produces XOR results correctly``() =
    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [4.7621; 4.7618] 7.3061;
                        Neuron.create [6.3917; 6.3917] 2.8441]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = HiddenLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsWithMomentum;
        }

    let outputLayer = { 
            Neurons = [ Neuron.create [-10.3788; 9.7691] 4.5589]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = OutputLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsWithMomentum;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let result = xorNetwork.Activate inputs.[0]
    result.[0] |> should (equalWithin 0.1 ) 0 // 0.0155 Note Perhaps do this, depends on form of "output" denormilization

    let result2 = xorNetwork.Activate inputs.[1]
    Math.Round (result2.[0], 4) |> should equal 0.9849

    let result = xorNetwork.Activate inputs.[2]
    Math.Round (result.[0], 4) |> should equal 0.9849

    let result = xorNetwork.Activate inputs.[3]
    Math.Round (result.[0], 4) |> should equal 0.0175     



[<Fact>]
let ``Two layer logistic sigmoid based network without momentum learns to produces XOR results correctly from preset starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = HiddenLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [ Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = OutputLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    //let trainedXORNetwork = trainNetworkWithEpocNTimes (inputs, desiredOutputs, 0.25, xorNetwork, 224)
    //let trainedXORNetwork = trainNetworkWithEpocNTimes (inputs, desiredOutputs, 0.1, xorNetwork, 56000)
    //let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 19.3, xorNetwork, 0.001, "sigmoid") // Produces "good results in low ish amount of iterations"
    
    // LR 2.25 = 24 epocs
    //let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 2.25, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"
    // LR 0.1 = 2806 epocs
    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"
    // ^^ With  momentum + learning rate adaption 100 epocs

    //let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 8.1, xorNetwork, 0.001)
    // let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.2, xorNetwork, 0.001) //Times out for h/tan AF..requires correct derivative

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

[<Fact>]
let ``Two layer sigmoid based network with momentum learns to produces XOR results correctly from random starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = Layer.Spawn 2 2 LogisticSigmoidFunction HiddenLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

[<Fact>]
let ``Two layer sigmoid based spawned network with momentum learns to produces XOR results correctly from random starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let  xorNetwork = Network.Spawn 2 1 2 HiddenLayerLogisticErrorGradientFunction 1 OutputLayerLogisticErrorGradientFunction LogisticSigmoidFunction adjustWeightsWithMomentum

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

[<Fact>]
let ``Two layer hyperbolic tangent based network with momentum learns to produces XOR results correctly from preset starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = HiddenLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = OutputLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }
    //let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 2.25, xorNetwork, 0.001)
    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 1.85, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    printf "\nDesired Network:\n%s" (SerializeToJson(trainedXORNetwork))
    //let persister = Persiter (location = "C:\\src\\Academia\\faif\\FAIF\\FAIF\\bin\\Debug\\")
    let persister = Persiter()
    do persister.saveNetwork (trainedXORNetwork, "network") |> ignore

    let retrievedNetwork = persister.openNetwork ("network")
    let index = 3
    let result = retrievedNetwork.Activate inputs.[index]
    printf "\n\nActual Network:\n%s" (SerializeToJson(retrievedNetwork))
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    

[<Fact>]
let ``Two layer hyperbolic tangent sigmoid based network without momentum learns to produces XOR results correctly from preset starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = HiddenLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = OutputLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    // LR of 0.3 takes 81 epocs (momentum added) when using std sigmoid with hyperbolic derivitive 
    // LR of 0.1 takes 26 epocs with hyperbolic derivitive and AF
    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Times out for h/tan AF..requires correct derivative
    // ^^ With learning momentum + rate adaption 27 epocs 
    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]


[<Fact>]
let ``Two layer hyperbolic tangent sigmoid based network with momentum learns to produces XOR results correctly from random starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = Layer.Spawn 2 2 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    
    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Times out for h/tan AF..requires correct derivative

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]


[<Fact>]
let ``Four layer hyperbolic tangent sigmoid based network with momentum learns to produce XOR results correctly from random starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer1 = Layer.Spawn 2 2 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer2 = Layer.Spawn 2 2 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let hiddenLayer3 = Layer.Spawn 2 2 HyperbollicTangentSigmoidFunction HiddenLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    
    //let hiddenLayers = produceHiddenLayers (2, 2, HyperbollicTangentSigmoidFunction, HiddenLayerHyperbollicTangentGradientFunction)
    
    //let outputLayer = Layer.Spawn 1 2 HyperbollicTangentSigmoidFunction OutputLayerHyperbollicTangentGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 
    //let thing = outputLayer::hiddenLayers

    let  xorNetwork = { Layers = [hiddenLayer1; hiddenLayer2; hiddenLayer3; outputLayer] }
    //let  xorNetwork = { Layers = List.concat(hiddenLayers, outputLayer) }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.022, xorNetwork, 0.01) // Times out for h/tan AF..requires correct derivative
    // ^^ With learning momentum + rate adaption 34 / 148 / 53 epocs - often times out
    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]



[<Fact>]
let ``Test 'trainNetworkWithInput' function trains as expected without momentum``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = HiddenLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = OutputLayerLogisticErrorGradientFunction
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  network = trainNetworkWithInput (inputs.[0], desiredOutputs.[0], {Layers = [hiddenLayer; outputLayer]}, 0.1)

    // Test hidden layer weights are adjusted correctly 
    let hiddenLayer = network.Layers.Head
    let hiddenNeuron1AdjustedWeight1 = hiddenLayer.Neurons.[0].Weights.[0]
    let hiddenNeuron1AdjustedWeight2 = hiddenLayer.Neurons.[0].Weights.[1]
    Math.Round (hiddenNeuron1AdjustedWeight1,4)  |> should equal 0.5038 
    Math.Round (hiddenNeuron1AdjustedWeight2,4)  |> should equal 0.4038 
    //
    let hiddenNeuron2AdjustedWeight1 = hiddenLayer.Neurons.[1].Weights.[0]
    let hiddenNeuron2AdjustedWeight2 = hiddenLayer.Neurons.[1].Weights.[1]
    Math.Round (hiddenNeuron2AdjustedWeight1,4)  |> should equal 0.8985
    Math.Round (hiddenNeuron2AdjustedWeight2,4)  |> should equal 0.9985
    //
    // Test hidden layer thresholdsa are adjusted correctly 
    let hiddenThresholds = [for neuron in hiddenLayer.Neurons do yield neuron.Threshold]

    let adjustedHiddenThreshold1 = hiddenThresholds.[0]
    let adjustedHiddenThreshold2 = hiddenThresholds.[1]
    Math.Round (adjustedHiddenThreshold1, 4) |> should equal 0.7962
    Math.Round (adjustedHiddenThreshold2, 4) |> should equal -0.0985

    // Test hidden layer weights are adjusted correctly 
    let outputLayer = network.Layers.Tail.Head
    let outputNeuron1AdjustedWeight1 = outputLayer.Neurons.[0].Weights.[0]
    let outputNeuron1AdjustedWeight2 = outputLayer.Neurons.[0].Weights.[1]
    Math.Round (outputNeuron1AdjustedWeight1,4)  |> should equal -1.2067 
    Math.Round (outputNeuron1AdjustedWeight2,4)  |> should equal 1.0888 
    //
    //
    // Test output layer thresholds are adjusted correctly 
    let outputThresholds = [for neuron in outputLayer.Neurons do yield neuron.Threshold]

    let adjustedOutputThreshold1 = outputThresholds.[0]
    Math.Round (adjustedOutputThreshold1, 4) |> should equal 0.3127


[<Fact>]
let ``Test layer training (outputs and adjustments, no momentum) based on Logistic Sigmoidal Neural Network from preset starting point``() =

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [0.0;1.0;1.0;0.0]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = HiddenLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = LogisticSigmoidFunction;
            GradientDescentFunction = OutputLayerLogisticErrorGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    // Activate with first input [1;1]
    let firstInput = inputs.[0]
    let firstDesiredOutput = desiredOutputs.[0]
    let reversedOutputs, reversedNetwork =  xorNetwork.ActivateForLearning (firstInput)
    
    let activatedOutputLayer = reversedNetwork.[0]
    let activatedHiddenLayer = reversedNetwork.[1]

    // Check outputs are as expected
    let hiddenOutput = reversedOutputs.[1]
    let y3 = hiddenOutput.[0]
    Math.Round (y3,4) |> should equal 0.5250
    let y4 = hiddenOutput.[1]
    Math.Round (y4,4) |> should equal 0.8808

    let networkOutput = reversedOutputs.[0]
    let y5 = networkOutput.[0]
    Math.Round (y5,4) |> should equal 0.5097
    
    let learningRate = 0.1
    let trainedOutputLayer, gradientAdjustmentRatios, outputErrorGradients, outputWeightAdjustments, outputThresholdAdjustments
        = trainLayer(activatedOutputLayer, hiddenOutput, networkOutput,  [firstDesiredOutput], learningRate )
            
    // Check output error gradient is calculated  correctly
    let outputErrorGradient = outputErrorGradients.[0]
    Math.Round (outputErrorGradient,4)  |> should equal -0.1274

    // Check output weight adjustment is calculated correctly
    let outputNeuronWeightAdjustment1 = outputWeightAdjustments.[0].[0]
    let outputNeuronWeightAdjustment2 = outputWeightAdjustments.[0].[1]
    Math.Round (outputNeuronWeightAdjustment1,4)  |> should equal -0.0067
    Math.Round (outputNeuronWeightAdjustment2,4)  |> should equal -0.0112

    // Test weights are adjusted correctly 
    let outputAdjustedWeight1 = trainedOutputLayer.Neurons.[0].Weights.[0]
    let outputAdjustedWeight2 = trainedOutputLayer.Neurons.[0].Weights.[1]
    Math.Round (outputAdjustedWeight1,4)  |> should equal -1.2067 
    Math.Round (outputAdjustedWeight2,4)  |> should equal 1.0888 
        
    // Test threshold adjustments 
    let outputThresholdAdjustment = outputThresholdAdjustments.[0]
    Math.Round (outputThresholdAdjustment, 4) |> should equal 0.0127
    let adjustedOutputThreshold = trainedOutputLayer.Neurons.[0].Threshold
    Math.Round (adjustedOutputThreshold , 4) |> should equal 0.3127


    let trainedHiddenLayer, hiddenGradientAdjustmentRatios, hiddenErrorGradients, hiddenWeightAdjustments, hiddenThresholdAdjustments
          = trainLayer (activatedHiddenLayer, firstInput, hiddenOutput, gradientAdjustmentRatios, learningRate )
    // Check output error gradient is calculated  correctly
    let hiddenErrorGradient1 = hiddenErrorGradients.[0]
    let hiddenErrorGradient2 = hiddenErrorGradients.[1]
    Math.Round (hiddenErrorGradient1,4)  |> should equal 0.0381
    Math.Round (hiddenErrorGradient2,4)  |> should equal -0.0147


    // Check output weight adjustment is calculated correctly
    // First Neuron
    let hiddenNeuron1WeightAdjustment1 = hiddenWeightAdjustments.[0].[0]
    let hiddenNeuron1WeightAdjustment2 = hiddenWeightAdjustments.[0].[1]
    Math.Round (hiddenNeuron1WeightAdjustment1,4)  |> should equal 0.0038
    Math.Round (hiddenNeuron1WeightAdjustment2,4)  |> should equal 0.0038
    // Second Neuron
    let hiddenNeuron2WeightAdjustment1 = hiddenWeightAdjustments.[1].[0]
    let hiddenNeuron2WeightAdjustment2 = hiddenWeightAdjustments.[1].[1]
    Math.Round (hiddenNeuron2WeightAdjustment1,4)  |> should equal -0.0015
    Math.Round (hiddenNeuron2WeightAdjustment2,4)  |> should equal -0.0015

    // Test weights are adjusted correctly 
    let hiddenNeuron1AdjustedWeight1 = trainedHiddenLayer.Neurons.[0].Weights.[0]
    let hiddenNeuron1AdjustedWeight2 = trainedHiddenLayer.Neurons.[0].Weights.[1]
    Math.Round (hiddenNeuron1AdjustedWeight1,4)  |> should equal 0.5038 
    Math.Round (hiddenNeuron1AdjustedWeight2,4)  |> should equal 0.4038 
    //
    let hiddenNeuron2AdjustedWeight1 = trainedHiddenLayer.Neurons.[1].Weights.[0]
    let hiddenNeuron2AdjustedWeight2 = trainedHiddenLayer.Neurons.[1].Weights.[1]
    Math.Round (hiddenNeuron2AdjustedWeight1,4)  |> should equal 0.8985
    Math.Round (hiddenNeuron2AdjustedWeight2,4)  |> should equal 0.9985

    // Test threshold adjustments 
    let hiddenThresholdAdjustment1 = hiddenThresholdAdjustments.[0]
    let hiddenThresholdAdjustment2 = hiddenThresholdAdjustments.[1]
    Math.Round (hiddenThresholdAdjustment1, 4) |> should equal -0.0038
    Math.Round (hiddenThresholdAdjustment2, 4) |> should equal 0.0015

    // Test thresholds are adjusted correctly {NOTE Don't need to send this back from training, it is contained within trained neuron}
    let adjustedHiddenThreshold1 = trainedHiddenLayer.Neurons.[0].Threshold
    let adjustedHiddenThreshold2 = trainedHiddenLayer.Neurons.[1].Threshold
    Math.Round (adjustedHiddenThreshold1, 4) |> should equal 0.7962
    Math.Round (adjustedHiddenThreshold2, 4) |> should equal -0.0985

    let adjustedHiddenThreshold1 = trainedHiddenLayer.Neurons.[0].Threshold
    let adjustedHiddenThreshold2 = trainedHiddenLayer.Neurons.[1].Threshold
    Math.Round (adjustedHiddenThreshold1, 4) |> should equal 0.7962
    Math.Round (adjustedHiddenThreshold2, 4) |> should equal -0.0985



[<Fact>]
let ``ReadCsvFile``() =
    let csvReader = CsvReader("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData : List<List<float>> = csvReader.openCsv ("cervicalcancer") 

    csvData.[0].[0] |> should equal 18.0
    csvData.[1].[1] |> should equal 1.0
    csvData.[857].[0] |> should equal 29.0


    let inputs, desiredOutput = getTrainingDataAndDesiredOutput (csvData, 28)

    inputs.[0].[0] |> should equal 18.0
    inputs.[1].[27] |> should equal 3.0
    inputs.[857].[26] |> should equal 4.0

    desiredOutput.[0].[0] |> should equal 0.0
    desiredOutput.[849].[0] |> should equal 1.0
    desiredOutput.[849].[2] |> should equal 1.0
    desiredOutput.[857].[7] |> should equal 0.0


[<Fact>]
let ``Learns XOR from CSV file`` () =

    let csvReader = CsvReader("C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata")
    let csvData : List<List<float>> = csvReader.openCsv ("xor") 
    
    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 2)

    let hiddenLayer = Layer.Spawn 2 2 LogisticSigmoidFunction HiddenLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    result.[0] |> should (equalWithin 0.1 ) desiredOutputs.[index].[0]