﻿module NormalizedNetwork
open Network
open Normalizer


type NormalizedNetwork = { Network:Network; Normalizer: Standardizer } with 
    

    member this.Activate (inputs:List<float>) : List<float> =

        let arayOfList = inputs |> List.map (fun x -> (string) x) |> List.toArray
        let normalizedInputs = this.Normalizer.GetStandardRow (arayOfList) |> Seq.toList
        this.Network.Activate normalizedInputs

    member this.getNormalizer = this.Normalizer
    member this.getNetwork = this.Network

//let create (network, normalizer) = { Network = network; Normalizer = normalizer }


