﻿module Teacher.TrainingFunctions
open Neuron
open Utils.Functions
open System

// Weight training constant
let inertia = 0.95


// Learning rate constants
let learningRateAdjustmentThreshold = 1.04
let learningRateDecreaseRatio = 0.7
let learningRateIncreaseRatio = 1.05             


// Neuron level error
let Error (desiredOutput:float, actualOutput:float) = desiredOutput - actualOutput

// Note : Naming clash between perceptron and network (delta vs error)
// Perceptron level error
let Delta (input, desiredOutput:float, actualOutput:float, learningRate) = 
   learningRate * (input*Error(desiredOutput, actualOutput))

// Network level error
let SumOfSquaredErrors (desiredOutputs : List<float>, actualOutputs :List<float>) : float =
    List.map2 (fun actualOutput desiredOutput -> Error (desiredOutput, actualOutput)**2.0) actualOutputs desiredOutputs
    |> List.reduce (+)

let SumOfSquaredErrorsOfEpoc (desiredOutputs, actualOutputs) = 
    List.map2 (fun desiredOutput actualOutput -> SumOfSquaredErrors (desiredOutput, actualOutput)) desiredOutputs actualOutputs
    |> List.reduce (+)

let NetworkPerformanceMetrics (actualOutputs :List<List<float>>, desiredOutputs :List<List<float>>) =
    let round (num:float) = Math.Round (num, 0)



    let individualOutputNodeMatches = 
        List.map2 (fun actuals desireds ->  
            List.map2 (fun actual desired -> round(actual) = desired) 
                actuals desireds ) 
            actualOutputs desiredOutputs
        |> List.concat            
        |> List.filter (fun isMatch -> isMatch = true)
        |> List.length
    

    let outputLayerBasedMatches = 
        List.map2 (fun actuals desireds ->  
            List.map2 (fun actual desired -> round(actual) = desired) actuals desireds ) 
            actualOutputs desiredOutputs
        |> List.map (fun row -> row |> List.fold (fun acc item -> acc && item) true)
        |> List.filter (fun isMatch -> isMatch = true)
        |> List.length

//    printf "Possitive Matches: %i \n" positiveMatches

    

    let individualOutputNodeBasedOutputCount = actualOutputs.Length * actualOutputs.Head.Length
    let individualOutputNodeBasedAccuracy = (float) individualOutputNodeMatches / (float) individualOutputNodeBasedOutputCount * 100.0
    let individualOutputNodeBasedMismatches = individualOutputNodeBasedOutputCount - individualOutputNodeMatches

    let outputLayerBasedAccuracy = (float) outputLayerBasedMatches / (float) actualOutputs.Length * 100.0

    //(individualOutputNodeBasedOutputCount, individualOutputNodeMatches, individualOutputNodeBasedMismatches, outputLayerBasedAccuracy, outputLayerBasedMatches)
    (individualOutputNodeBasedAccuracy, outputLayerBasedAccuracy)
  

let AdjustPerceptronWeight (weight, input, desiredOutput: float, actualOutput: float, learningRate) = 
    (Delta(input, desiredOutput, actualOutput, learningRate) + weight)



let AdjustLearningRate (learningRate, squaredErrorSum, previousSquaredErrorSum) =
            
    if (squaredErrorSum/previousSquaredErrorSum > learningRateAdjustmentThreshold) 
    then learningRate*learningRateDecreaseRatio
    else learningRate*learningRateIncreaseRatio


let adjustWeightsWithMomentumV2 (neurons, inputs, learningRate, errorGradients) = 
    // From Wikipedia has : (1 - momentum)(lr * input * eg) + momentum*previousWeightAdjustment (NOTE: not converging)
    let weightAdjustments = List.map2 ( fun errorGradient neuron -> List.map2 ( fun input previousWeightAdjustment -> (1.0 - inertia) + (learningRate * input * errorGradient) + (inertia * previousWeightAdjustment)) inputs neuron.PreviousWeightAdjustments ) errorGradients neurons
    let adjustedWeights = 
        List.map2 (fun neuron neuronAdjustments -> List.map2 (fun weight adjustment -> weight + adjustment) neuron.Weights neuronAdjustments) neurons weightAdjustments

    let thresholdAdjustments = 
        List.map2 (fun errorGradient neuron -> (1.0 - inertia)  + (learningRate * -1.0 * errorGradient) +   (inertia * neuron.PreviousThresholdAdjustment)) errorGradients neurons
    let adjustedThresholds = 
        List.map2 (fun neuron thresholdAdjustment -> neuron.Threshold + thresholdAdjustment) neurons thresholdAdjustments
    
    weightAdjustments, adjustedWeights, thresholdAdjustments, adjustedThresholds



let adjustWeightsWithMomentum (neurons, inputs, learningRate, errorGradients) = 

    let weightAdjustments = List.map2 ( fun errorGradient neuron -> List.map2 ( fun input previousWeightAdjustment -> inertia * previousWeightAdjustment + (learningRate * input * errorGradient)) inputs neuron.PreviousWeightAdjustments ) errorGradients neurons
    let adjustedWeights = 
        List.map2 (fun neuron neuronAdjustments -> List.map2 (fun weight adjustment -> weight + adjustment) neuron.Weights neuronAdjustments) neurons weightAdjustments

    let thresholdAdjustments = 
        List.map2 (fun errorGradient neuron -> inertia * neuron.PreviousThresholdAdjustment + (learningRate * -1.0 * errorGradient)) errorGradients neurons
    let adjustedThresholds = 
        List.map2 (fun neuron thresholdAdjustment -> neuron.Threshold + thresholdAdjustment) neurons thresholdAdjustments
            
    weightAdjustments, adjustedWeights, thresholdAdjustments, adjustedThresholds   


let adjustWeightsSimple (neurons, inputs, learningRate, errorGradients) = 
    // Wikipedia has a variation on this: (1 - momentum)(lr * input * eg) + momentum*previousWeightAdjustment
    let weightAdjustments = List.map2 ( fun errorGradient neuron -> List.map2 ( fun input previousWeightAdjustment -> (learningRate * input * errorGradient)) inputs neuron.PreviousWeightAdjustments ) errorGradients neurons
    let adjustedWeights = 
        List.map2 (fun neuron neuronAdjustments -> List.map2 (fun weight adjustment -> weight + adjustment) neuron.Weights neuronAdjustments) neurons weightAdjustments

    let thresholdAdjustments = 
        List.map2 (fun errorGradient neuron -> (learningRate * -1.0 * errorGradient)) errorGradients neurons
    let adjustedThresholds = 
        List.map2 (fun neuron thresholdAdjustment -> neuron.Threshold + thresholdAdjustment) neurons thresholdAdjustments
    
    weightAdjustments, adjustedWeights, thresholdAdjustments, adjustedThresholds




let generateGradientAdjustmentRatios (neurons, errorGradients) = 
    let transposedWeights = [for neuron in neurons do yield neuron.Weights] |> transpose 
    let backPropogationErrorGradientsMultipliedByWeights = 
        List.map (fun listOfWeights ->  List.map2 (fun weight errorGradient -> weight * errorGradient) listOfWeights errorGradients) transposedWeights
    let gradientAdjustmentRatiosForBackPropagation = 
        backPropogationErrorGradientsMultipliedByWeights |> List.map (fun row ->  row |> List.reduce (+))
    
    gradientAdjustmentRatiosForBackPropagation