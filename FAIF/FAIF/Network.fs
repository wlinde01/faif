﻿module Network
open Layer
open Utils.Functions


type Network = { Layers:List<Layer> } with 
    

    (*  Activating a Network
        - Take input as first Activation against first layer (which will be the input Layer)
        - loop through Layers, apply activation result of previous layer to activation of next layer    *)
    member this.Activate (inputs:List<float>) : List<float> =

        let rec activateAllLayers (layers: List<Layer>, previousOutput: List<float>) : List<float> = 
            match layers with                 
                | h::t -> activateAllLayers(t, h.Activate(previousOutput))
                | [] -> previousOutput

        activateAllLayers (this.Layers, inputs)


    member this.ActivateForLearning (inputs:List<float>) =

        let rec activateAllLayers (layers: List<Layer>, previousOutputs: List<List<float>>, reversedLayers : List<Layer>) = 
            match layers with                 
                | h::t -> activateAllLayers(t, h.Activate(previousOutputs.Head)::previousOutputs, h::reversedLayers)
                | [] -> (previousOutputs, reversedLayers)

        activateAllLayers (this.Layers, [inputs], [])


let create (layers) = { Layers = layers; }

let Spawn   inputCount
            hiddenLayerCount hiddenLayerNeuronCount hiddenLayerGradientDescentFunction
            outputLayerNeuronCount outputLayerGradientDescentFunction
            activationFunction  weightTrainingFunction  = 

        let spawnHidden = Layer.Spawn hiddenLayerNeuronCount inputCount activationFunction hiddenLayerGradientDescentFunction weightTrainingFunction 

        let rec generateHiddenLayers (counter : int, layers:List<Layer>) = 
            match counter with
            | 0 -> layers
            | _ -> generateHiddenLayers (counter - 1, spawnHidden::layers)
    
        let outputLayer = Layer.Spawn outputLayerNeuronCount inputCount activationFunction outputLayerGradientDescentFunction weightTrainingFunction        
        let networkLayers = generateHiddenLayers (hiddenLayerCount, [outputLayer])
        
        { Layers = networkLayers }
    
