﻿module Neuron
open Utils.Functions

// Note: Lazy evaluation
// Note: What about using Trees?

// Note: ToDo: Generics here - unless all is generic this is probably pointless  
type Neuron = { Weights:List<float>; PreviousWeightAdjustments:List<float>; Threshold:float; PreviousThresholdAdjustment:float } with 

    member this.Activate (inputs:List<float>, activationFunction) = 
        
        //printf "Neuron: " 
        let SumByWeights (inputs:List<float>) = 
                let rec sumByWeights (sum:float, inputs:List<float>, weights:List<float>) =
                    match inputs, weights with
                    | ih::it, wh::wt -> sumByWeights ((ih*wh  + sum), it, wt)
                    | _ -> sum 
        
                (sumByWeights(0.0, inputs,this.Weights) - this.Threshold)

//        printf "In: [" 
//        inputs |> Seq.iteri (fun i x -> printf "%f@%f;" x inputs.[i])
//        printf "] -> " 

        let weightedSum = SumByWeights (inputs)
        let output = activationFunction weightedSum
        //printf "[%f]\n" output

        
        activationFunction weightedSum



let create weights threshold = { 
    Weights = weights; 
    Threshold = threshold; 
    PreviousWeightAdjustments = [for i in 1..weights.Length do yield 0.0];
    PreviousThresholdAdjustment = 0.0
    }

let createWithWeightAdjustments weights previousWeightAdjustments threshold previousThresholdAdjustment = { 
    Weights = weights; 
    Threshold = threshold; 
    PreviousWeightAdjustments = previousWeightAdjustments;
    PreviousThresholdAdjustment = previousThresholdAdjustment
    }

// Spwan: Creates random initial weights and threshold based on number of inputs to neuron (Haykin 1999)
// Note: Since inputCount is not a member, should this be a diffirent thing, or a seperate function that simply returns a spawned Neuron?
let Spawn inputCount =  
    let weights = [for i in 1..inputCount do yield GetRandomHaykin(inputCount) ];
    let threshold = GetRandomHaykin(inputCount)

    { 
        Weights = weights; 
        Threshold = threshold; 
        PreviousWeightAdjustments = [for i in 1..weights.Length do yield 0.0];
        PreviousThresholdAdjustment = 0.0
    }


// ToDo: Deal with this - Move into Layer.fs
// Note: Consider using sequences (good if not all elements are used, computes 

//type Layer (perceptrons:Set<int>) =
//    member this.perceptrons = perceptrons

//let perceptrons : Set<Perceptron> =  
//    [for i in 1..3 do yield Perceptron(Set([1..9]),Set([9..1]))]
