TODOS
-----

ToDo: Persist Normilisation Parameters for using against trained network
ToDo: Enhance network Spawn (deduce network configuration from input/output)
ToDo: Create simple util / cmd to use network
ToDo: Better way to specify error margin (base on test accuracy)

ToDo: Introduce specflow and simple example to produce “persisted” network
ToDo: + Introduce better persist/recall mechanisms 
ToDo: Build console application 
ToDo: Cervical Cancer Example
ToDo: Identify network oscilation, work around this...stop or restart with alternative random weights
ToDo: Perhaps find an example network configuration online, create network from it, show it produces correct output
ToDo: Overide Network tostring to show json: override x.ToString() = sprintf "%A" x (and associated functions)
ToDo: Remove one of the Delta functions (lives in two places...teacher and one of the util modules)
ToDo: Configuration validator 
ToDo: Normilzation
ToDo: Fix name spacing (make all of it FAIF. based)

- Refactor [?]:	
	- Make it possible to activate / deactivate  (Not sure if this is required, perhaps ask)
		+ Delta momentum (depends on the function passed in)
		- Learning rate adjustment	


- Metrics.. .how to deal with those.. Make them part of the network?
- Consider persisting network state
- How to detect an oscilating network in training? 
- Input / Output layers (What to do, normilizer vs demormilazing??)

- Should weight and threshold adjustment functions live in the Layer, on a per neuron / weight level? ...or the Network even?


Notes For Report:
    // ToDo: Note, testing causing problems with none-converging networks. ..tests are not guarenteed to produce converging netwrok, 
    // this means that you can end up waiting for test runs with no good / bad answer ... lot of test failures are simply 
    // random network weight initialisation not resulting in a "timely" convergent or "within error" margin network

    // ToDo: Note on test data: Simplified outputs work well.. what happens when 
    // A: produce a NN per desired output (so one NN per Citology one for	Biopsy etc...
    // B: produce a NN with multiple outputs, (so one output neuron per Citology one for Biopsy etc...

    // NB: Note in lessons learned : Know your data!!!!


+/- Gather problems to solve using the backpropagation NN and do research around convolution problems etc.
+/- Remove all "constants" and introduce parameterisation (depends on final architecture, )
+/- Check results when using a hyperbolic tangent Sigmoidal activation function
	- Have used std hyperbolic tangent, but textbook version lacks derivative for back propegation

+ Introduce network Spawning // Simplify tests around this
+ Refactors
	+ Change "multilayer" tests to "Two layer" tests
	+ AF & 'AF "injections".. both should be part of the Layer it is used against
	+ Move weight training into extrenal function (and potentially other bits from layer training)

+ Create multilayer XOR network and compare results
+ Add in adaptive learning rate mechanism (page 187)
	- Require we keep track of previous iteration's squared error 
+ Add in momentum into the delta rule (page 185 / 6.17)
	- Requires we add previous weight adjustments into "layer" for next learning iteration's use
+ Check results when rounding to 4 digits (no affect)
+ Refactor Perceptron to Neuron (Perceptron is a single neuron neural network)