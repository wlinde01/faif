﻿module Teacher.Network
open Teacher.TrainingFunctions
open Utils.Functions
open Layer
open Network


let trainLayer (layer : Layer, relativeInputs:List<float>, layerOutputs : List<float>, desiredOutputsOrErrorGradients : List<float>, learningRate) =
    
    let layerNeurons = layer.Neurons    
    let errorGradientFunction = layer.GradientDescentFunction
    let weightTrainFunction = layer.WeightTrainingFunction

    let errorGradients = 
        List.map2 (fun actualOutput desiredOutput -> errorGradientFunction(desiredOutput, actualOutput)) layerOutputs desiredOutputsOrErrorGradients

    let weightAdjustments, adjustedWeights, thresholdAdjustments, adjustedThresholds  = weightTrainFunction (layerNeurons, relativeInputs, learningRate, errorGradients)


    let gradientAdjustmentRatiosForBackPropagation  = generateGradientAdjustmentRatios (layerNeurons, errorGradients)
    let trainedLayer =
        map4 (fun weights threshold weightAdjustments thresholdAdjustment -> Neuron.createWithWeightAdjustments weights weightAdjustments threshold thresholdAdjustment)
            adjustedWeights adjustedThresholds weightAdjustments thresholdAdjustments

    // Note don't need to return adjusted thresholds, they are within the trained layer
    (Layer.create (trainedLayer, layer.ActivationFunction, layer.GradientDescentFunction, weightTrainFunction), gradientAdjustmentRatiosForBackPropagation, errorGradients, weightAdjustments, thresholdAdjustments )
     
// Train network based on a single collection of inputs and a corresponding collection of desired outputs
let trainNetworkWithInput (input, desiredOutput, network : Network, learningRate) =
        
    let reversedActivationOutputs, reversedLayers = network.ActivateForLearning (input)
    let outputLayer = reversedLayers.Head

    let relativeInputLayer = reversedLayers.Tail.Head

    let relativeInputs = reversedActivationOutputs.Tail.Head
    let actualOutput = reversedActivationOutputs.Head

    // let trainOutputLayer  (relativeInputs:List<float>, actualOutputs : List<float>, desiredOutputs : List<float>, relativeOutputLayer: Layer, errorGradientFunction) =
    // ToDo / NOTE ...unused variable.... leave it exposed for testing??
    // Train the output layer
    let trainedOutputLayer, gradientAdjustmentRatios, _,_,_ = trainLayer (outputLayer, relativeInputs, actualOutput, desiredOutput, learningRate )

    let rec trainHiddenLayers (hiddenLayer : Layer, remainingHiddenLayers : List<Layer>, relativeInputs : List<List<float>>, trainedLayers : List<Layer>, gradientAdjustmentRatios) =
        let actualOutput = relativeInputs.Head
        let relativeInput = relativeInputs.Tail.Head
        // (relativeInputs:List<float>, actualOutputs : List<float>, gradientAdjustmentRatios : List<float>, relativeOutputLayer: Layer, errorGradientFunction) =
        //                                                                               (hiddenLayer: Layer, relativeInputs:List<float>, actualOutputs : List<float>, gradientAdjustmentRatios : List<float>, errorGradientFunction, learningRate) =
        let trainedHiddenLayer, nextGradientAdjustmentRatios, _,_,_ = trainLayer ( hiddenLayer, relativeInput, actualOutput, gradientAdjustmentRatios, learningRate )
        match remainingHiddenLayers with
        //| h::t when (t.Length < 1) -> trainedLayers
        | newHiddenLayer::newRemainingHiddenLayers -> trainHiddenLayers(newHiddenLayer, newRemainingHiddenLayers, relativeInputs.Tail, trainedHiddenLayer::trainedLayers, nextGradientAdjustmentRatios)
        | [] ->trainedHiddenLayer::trainedLayers

    let remainingLayers = reversedLayers.Tail
    let remainingRelativeInputs = reversedActivationOutputs.Tail
    let hiddenLayer = remainingLayers.Head // start with the second last layer as that is the first hidden layer
    let remainingHiddenLayers = remainingLayers.Tail

    let trainedLayers = trainHiddenLayers (hiddenLayer, remainingHiddenLayers, remainingRelativeInputs, [trainedOutputLayer], gradientAdjustmentRatios)
    Network.create trainedLayers

// Go through each input from epoc, and train network based on desired output
let rec trainNetworkWithInputs (inputs : List<List<float>>, desiredOutputs : List<List<float>>, network : Network, learningRate) = 


    match inputs, desiredOutputs with 
    | inputs, outputs when (inputs.Length <= 0 || outputs.Length <= 0)-> network
    | inHead::inTail, outHead::outTail -> trainNetworkWithInputs (inTail, outTail, trainNetworkWithInput(inHead, outHead, network, learningRate), learningRate)



// Train a network for a predefined set of iterations
let trainNetworkWithEpocNTimes(epoc:List<List<float>>, desiredOutputs:List<List<float>>, learningRate, network : Network, n, activationType) =
    let rec trainNetworkNTimes (network, n) = 
        match n with 
        | n when n  <= 0 -> network
        | n -> trainNetworkNTimes (trainNetworkWithInputs (epoc, desiredOutputs, network, learningRate), n-1)

    trainNetworkNTimes (network, n)



// Train a network until it performs within a specified error margin
let trainNetworkWithEpocUntilErrorIswithinMargin (epoc, desiredOutputs, learningRate, network:Network, errorMargin) =         
    
    let rec trainWithInputs (epoc:List<List<float>>, network:Network, epocCounter, previousSquaredErrorSum, learningRate) =  
            
            if (previousSquaredErrorSum < errorMargin) 
            then   
                let outputs = List.map (fun input -> network.Activate(input)) epoc
                let nodeBasedAccuracy, outputLayerBasedAccuracy = NetworkPerformanceMetrics (outputs, desiredOutputs)
                printf "Training Complete: Epocs: %i Iterations: %i Error: %f Node based Accuracy: %f OutputLayer based Accuracy: %f  Learning Rate: %f\n" epocCounter (epocCounter*epoc.Length) previousSquaredErrorSum nodeBasedAccuracy outputLayerBasedAccuracy learningRate
                network                    
            else              
                let networkInTraining = trainNetworkWithInputs (epoc, desiredOutputs, network, learningRate)
                let outputs = List.map (fun input -> networkInTraining.Activate(input)) epoc
                let squaredErrorSum = SumOfSquaredErrorsOfEpoc (desiredOutputs, outputs)
                let newLearningRate = AdjustLearningRate (learningRate, squaredErrorSum, previousSquaredErrorSum)
                let nodeBasedAccuracy, outputLayerBasedAccuracy  = NetworkPerformanceMetrics (outputs, desiredOutputs)

                printf "In Training: Epocs: %i Iterations: %i Error: %f Node Accuracy: %f OutputLayer Accuracy: %f  Learning Rate: %f\n" epocCounter (epocCounter*epoc.Length) previousSquaredErrorSum nodeBasedAccuracy outputLayerBasedAccuracy learningRate
                trainWithInputs (epoc, networkInTraining, epocCounter + 1, squaredErrorSum, newLearningRate)          

    let outputs = List.map (fun input -> network.Activate(input)) epoc
    let squaredErrorSum = SumOfSquaredErrorsOfEpoc (desiredOutputs, outputs)
    trainWithInputs (epoc, network, 0, squaredErrorSum, learningRate) 


// This function was supposed to train a network based on accuracy % / works, but see note -->>
// Works but is not effective. "% accuracy varies greatly depending on what sort of output and what is considered a 100% correct output"
let trainNetworkWithEpocUntilNetworkHasAccuracy (epoc, desiredOutputs, learningRate, network:Network, performancePercentage) =
       let rec trainWithInputs (epoc:List<List<float>>, desiredOutputs, network:Network, epocCounter, previousSquaredErrorSum, previousPerformancePercentage, learningRate) =  
            if (previousPerformancePercentage > performancePercentage) 
            then   
                printf "Epocs: %i / Iterations: %i / Performance: %f\n" epocCounter (epocCounter*epoc.Length) previousPerformancePercentage
                network                    
            else              
                let networkInTraining = trainNetworkWithInputs (epoc, desiredOutputs, network, learningRate)
                let outputs = List.map (fun input -> networkInTraining.Activate(input)) epoc
                let squaredErrorSum = SumOfSquaredErrorsOfEpoc (desiredOutputs, outputs)
                
                let newLearningRate = AdjustLearningRate (learningRate, squaredErrorSum, previousSquaredErrorSum)
                let nodeBasedAccuracy, outputLayerBasedAccuracy  = NetworkPerformanceMetrics (outputs, desiredOutputs)
                printf "Training Complete: Epocs: %i / Iterations: %i Error: %f Node based Accuracy: %f OutputLayer based Accuracy: %f  Learning Rate: %f\n" epocCounter (epocCounter*epoc.Length) previousSquaredErrorSum nodeBasedAccuracy outputLayerBasedAccuracy learningRate
                trainWithInputs (epoc, desiredOutputs, networkInTraining, epocCounter + 1, squaredErrorSum, outputLayerBasedAccuracy, newLearningRate)          

       let outputs = List.map (fun input -> network.Activate(input)) epoc
       let nodeAccuracy, layerAccuracy = NetworkPerformanceMetrics (outputs, desiredOutputs)
       let squaredErrorSum = SumOfSquaredErrorsOfEpoc (desiredOutputs, outputs)
       trainWithInputs (epoc, desiredOutputs, network, 0, squaredErrorSum, layerAccuracy, learningRate) // Note, start with an arbitrarily high previous error sum, probably needs rethinking 
 
    




// POC:         
//    let input = inputs.Head
//    let desiredOutput = desiredOutputs.Head
//    let reversedOutputs, reversedLayers = network.ActivateForLearning (input)
//    let outputLayer = reversedLayers.Head
//    let relativeInputs = reversedOutputs.Head
//    //    let trainOutputLayer (relativeInputs:List<float>, actualOutputs : List<float>, desiredOutputs : List<float>, relativeOutputLayer: Layer, errorGradientFunction) =
//    let trainedOutputLayer, gradientAdjustmentRatios = trainOutputLayer ( relativeInputs, reversedOutputs.Head, desiredOutput, outputLayer, OutputLayerErrorGradient )
//
//
//    let outputLayer = reversedLayers.Tail.Head
//    let relativeInputs = reversedOutputs.Tail.Head
//    //let trainHiddenLayer (relativeInputs:List<float>, actualOutputs : List<float>, gradientAdjustmentRatios : List<float>, relativeOutputLayer: Layer, errorGradientFunction) =
//    let trainedHiddenLayer, gradientAdjustmentRatios = trainHiddenLayer ( relativeInputs, reversedOutputs.Tail.Head, gradientAdjustmentRatios, outputLayer, HiddenLayerErrorGradient )
    

    