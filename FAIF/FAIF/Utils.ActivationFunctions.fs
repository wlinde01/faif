﻿module Utils.ActivationFunctions
open Teacher.TrainingFunctions
open Utils.Functions

// AFs and their derivatives: https://en.wikipedia.org/wiki/Activation_function
// Some good info here: https://theclevermachine.wordpress.com/2014/09/08/derivation-derivatives-for-common-neural-network-activation-functions/
// Activation Functions :

// let IdentityFunction x = x // << Removed, as not currently used
let StepFunction x = if x >= 0.0 then 1.0 else 0.0

// Logistic sigmoidal Activation Fucntion
let e = System.Math.E
let LogisticSigmoidFunction x = 
    1.0 / (1.0 + e**(-x))

// Logistic Sigmoid derivative 
let OutputLayerLogisticErrorGradientFunction (desiredOutput:float, actualOutput:float) = 
    actualOutput * (1.0 - actualOutput) * Error (desiredOutput, actualOutput)

let HiddenLayerLogisticErrorGradientFunction (gradientAdjustmentRatio:float, actualOutput:float) = 
    actualOutput * (1.0 - actualOutput) * gradientAdjustmentRatio


// Hyperbollic Tangent Signmoidal Activation Fucntion
let HyperbollicTangentSigmoidFunction x = 
   (2.0/(1.0 + e**(-2.0*x))) - 1.0
    // let a = 1.716 // << These two require more research, to produce relevant derivative
    // let b = 0.667
    // (2.0*a / (1.0 + e**(-b*x))) - a (textbook version)

// Hyperbollic Tangent Signmoidal derivative 
let OutputLayerHyperbollicTangentGradientFunction (desiredOutput:float, actualOutput:float) = 
    (1.0 - actualOutput**2.0) * Error (desiredOutput, actualOutput)

let HiddenLayerHyperbollicTangentGradientFunction (gradientAdjustmentRatio:float, actualOutput:float) = 
    (1.0 - actualOutput**2.0) * gradientAdjustmentRatio


// Note : Naming clash between perceptron and network (delta vs error)
let Delta (input, desiredOutput:float, actualOutput:float, learningRate) = 
   learningRate * (input*Error(desiredOutput, actualOutput))

let AdjustPerceptronWeight (weight, input, desiredOutput: float, actualOutput: float, learningRate) = 
    (Delta(input, desiredOutput, actualOutput, learningRate) + weight)


