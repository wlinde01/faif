﻿module Utils.Functions


// Note: Best scope for this, should it be inside GetRandomHaykin?
let randomGenerator = new System.Random()
// (Haykin, 1999) - Note : check if bounderies are inclusive
let GetRandomHaykin (inputCount:int) = 
    let min = -2.4/float(inputCount)
    let max = 2.4/float(inputCount)
    let result = randomGenerator.NextDouble() * (max - min) + min
    //printf "Result: %f \n" result
    result


// https://stackoverflow.com/questions/3016139/help-me-to-explain-the-f-matrix-transpose-function
let rec transpose = function
    | (_::_)::_ as M -> List.map List.head M :: transpose (List.map List.tail M)
    | _ -> []


// From https://stackoverflow.com/questions/42901373/f-concise-way-to-define-map4-using-map2-and-map3
let inline (<!>) f xList = List.map f xList
let inline (<*>) gList xList = List.map2 (id) gList xList
let map4 f w x y z = f <!> w <*> x <*> y <*> z
