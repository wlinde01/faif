﻿module Layer
open Neuron
open Utils.Functions


type Layer = { 
    Neurons:List<Neuron>; 
    ActivationFunction : float -> float; 
    GradientDescentFunction : float*float -> float; 
    WeightTrainingFunction : List<Neuron>*List<float>*float*List<float> -> List<List<float>>*List<List<float>>*List<float>*List<float>; 
} with 
    
    member this.Activate (inputs:List<float>) : List<float> =
        let rec activateAllNeurons (list:List<Neuron>) : List<float> = 
            match list with 
                | h::t -> h.Activate (inputs, this.ActivationFunction) :: activateAllNeurons t
                | [] -> []
        
        activateAllNeurons this.Neurons


let create (neurons, activationFunction, gradientDescentFunction, weightTrainingFunction ) = { 
    Neurons = neurons; 
    ActivationFunction = activationFunction; 
    GradientDescentFunction = gradientDescentFunction;
    WeightTrainingFunction = weightTrainingFunction;
}


let Spawn neuronCount inputCount activationFunction gradientDescentFunction weightTrainingFunction = 
    let neurons = [for i in 1..neuronCount do yield Neuron.Spawn(inputCount)]
    { 
        Neurons = neurons; 
        ActivationFunction = activationFunction; 
        GradientDescentFunction = gradientDescentFunction; 
        WeightTrainingFunction = weightTrainingFunction; 
    }