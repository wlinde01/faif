﻿module Teacher.Perceptron
open Teacher.TrainingFunctions
open Utils.Functions
open Utils.ActivationFunctions
open Neuron
open Layer
open Network

let TrainPerceptron (perceptron:Neuron, inputs:List<float>, desiredOutput:float, threshold:float, learningRate) = 
    // recurse through weights and adjust based on e(p) = Yd(p) - Y(p)
    let actualOutput = perceptron.Activate(inputs, StepFunction)
    let rec adjustWeights (weights:List<float>, inputs:List<float>) = 
        match weights, inputs with
        |   weight::t, input::it -> 
            AdjustPerceptronWeight(weight, input, desiredOutput, actualOutput, learningRate)::adjustWeights(t, it)
        | _ -> []
            
    { perceptron with Weights = adjustWeights (perceptron.Weights, inputs) }


let TrainPerceptronWithEpoc (perceptron:Neuron, inputs : List<List<float>>, desiredOutputs :List<float>, threshold:float, learningRate) =
    
    let rec trainWithEpoc (perceptron:Neuron, inputs : List<List<float>>, desiredOutputs :List<float>) = 
        match inputs, desiredOutputs with
        | input::inTail, desOut::desOutTail -> trainWithEpoc (TrainPerceptron(perceptron, input, desOut, threshold, learningRate), inTail, desOutTail)
        | _ -> perceptron

    trainWithEpoc (perceptron, inputs, desiredOutputs)
    

let trainPerceptronWithEpocUntil (inputs:List<List<float>>, desiredOutputs:List<float>, initialWeights, count, threshold:float, learningRate, activationFunction) = 
    let initialPerceptron = Neuron.create initialWeights threshold
    
    // ToDo: Circuit breaker -> When Delta remains 0 / 
    let rec loop (perceptron, count) = 
        match count with 
        | count when count  <= 0 -> perceptron
        | count -> loop (TrainPerceptronWithEpoc(perceptron, inputs, desiredOutputs, threshold, learningRate), count-1)
    
    loop (initialPerceptron, count)

// Note: recursively loop through layer's neurons, train every one 
let trainLayerWithEpocUntil(inputs:List<List<float>>, desiredOutputs:List<float>, initialWeights, neuronCount, count, threshold:float, learningRate, activationFunction) : Layer =
    
    let neurons  = [for i in 1..neuronCount do yield Neuron.create initialWeights threshold]    
    let untrainedLayer = Layer.create (neurons , StepFunction, OutputLayerLogisticErrorGradientFunction, adjustWeightsSimple) // Note : Make more eligant OutputLayerLogisticErrorGradientFunction just a place holder, not being used
     // ToDo: This is a duplicate of trainWithEpocUntil's recursive "loop" 
    let rec trainNeuron (perceptron, count) = 
        match count with 
        | count when count  <= 0 -> perceptron
        | count -> trainNeuron (TrainPerceptronWithEpoc(perceptron, inputs, desiredOutputs, threshold, learningRate), count-1)

    Layer.create (
        untrainedLayer.Neurons 
        // Note : Make more eligant OutputLayerLogisticErrorGradientFunction just a place holder, not being used
        |> List.map ( fun neuron -> trainNeuron (neuron, count) ) , StepFunction, OutputLayerLogisticErrorGradientFunction, adjustWeightsSimple  
    )

