﻿module Utils.IO

open System
open System.Runtime.Serialization.Formatters.Binary
open System.IO
open Newtonsoft.Json
open FSharp.Data
open Normalizer
open NormalizedNetwork

open Network


let SerializeToJson (network : Network)  =
    JsonConvert.SerializeObject(network)

let SerializeToJson2 (network : NormalizedNetwork)  =
    JsonConvert.SerializeObject(network)

let SerializeBinary (network, file : string) =
    use stream = new FileStream (file, FileMode.Create)
    (new BinaryFormatter()).Serialize(stream, network)


let DeserializeBinary (file: string) =
    use stream = new FileStream (file, FileMode.Open)
    (new BinaryFormatter()).Deserialize(stream) |> unbox

let constructFileNameAndLocation (fileName, finalLocation, extension) = 
    let fileDetails = String.concat "\\" [ finalLocation; fileName;]
    String.concat "." [fileDetails; extension]

type Persiter (?location : string) = 
    
    let finalLocation = defaultArg location System.Environment.CurrentDirectory

    member this.saveNetwork(network : Network, ?fileName : string) = 
        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        do SerializeBinary (network, constructFileNameAndLocation(finalFilename, finalLocation, "faif"))

    member this.openNetwork (?fileName : string) : Network =
        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        DeserializeBinary (constructFileNameAndLocation(finalFilename, finalLocation, "faif"))

    member this.saveNormalizedNetwork(network : NormalizedNetwork, ?fileName : string) = 
        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        do SerializeBinary (network, constructFileNameAndLocation(finalFilename, finalLocation, "faif"))

    member this.openNormalizedNetwork (?fileName : string) : NormalizedNetwork =
        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        DeserializeBinary (constructFileNameAndLocation(finalFilename, finalLocation, "faif"))



//type csv = CsvProvider<"C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata\\cervicalcancer.csv">
//  RED type Csv (location : string) =     CsvProvider<"C:\\src\\Academia\\faif\\FAIF\\FAIF\\trainingdata\\cervicalcancer.csv">

type CsvReader (?location : string) = 
    
    let finalLocation = defaultArg location System.Environment.CurrentDirectory

    // Conventions@
    // First row contains column headings << ?? Really
    // Second row contains default values (used for missing data) ASUMES all data castable to float, if not BOOM!
    member this.openCsv (?fileName : string) : List<List<float>> =

        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        let finalFileAndLocation = constructFileNameAndLocation(finalFilename, finalLocation, "csv") // Nasty, rework extension generation convention
        


        let csv = CsvFile.Load(finalFileAndLocation).Cache()
        let header = csv.Headers // Do we need this??

        let csvMatrix : List<List<string>> = 
            let rows = Seq.toList(csv.Rows)
            rows 
            |> List.map (fun row -> 
                Array.toList(row.Columns)) 

        //let finalOutPutIndex = defaultArg requiredOutputIndex (csvMatrix.Head.Length - 1) // Asumes last col is only output field if no index is present

        let defaultValues = csvMatrix.Head
        let csvData = csvMatrix.Tail

        let getItemFloatSafe (item, defaultIndex) =
            try
                float item
            with
            | :? System.FormatException as ex -> float defaultValues.[defaultIndex]


        csvData |> List.map (fun row -> 
                row |> List.mapi (fun i item -> getItemFloatSafe (item, i) )) // Drop "index" if not used...should CSV reader be aware??


type CsvReaderWithNormilization (?location : string) = 
    
    let finalLocation = defaultArg location System.Environment.CurrentDirectory

    // Conventions@
    // First row contains column headings << ?? Really
    // Second row contains default values (used for missing data) ASUMES all data castable to float, if not BOOM!
    member this.openCsv (outPutIndex : int, ?fileName : string)  =

        let now = System.DateTime.Now.ToLongTimeString()
        let finalFilename = defaultArg fileName now
        let finalFileAndLocation = constructFileNameAndLocation(finalFilename, finalLocation, "csv") // Nasty, rework extension generation convention
        


        let csv = CsvFile.Load(finalFileAndLocation).Cache()
        let header = csv.Headers // Do we need this??

        let csvMatrix : List<List<string>> = 
            let rows = Seq.toList(csv.Rows)
            rows 
            |> List.map (fun row -> 
                Array.toList(row.Columns)) 

        //let finalOutPutIndex = defaultArg requiredOutputIndex (csvMatrix.Head.Length - 1) // Asumes last col is only output field if no index is present
        let collumnTypes = csvMatrix.Head
        let defaultValues = csvMatrix.Tail.Head
        let csvData = csvMatrix.Tail.Tail

        let getItemOrDefault (item : string, defaultIndex) =
            if (item.Length <= 0 || item = "?") then defaultValues.[defaultIndex]
                else item
           
        // ToDo: Sorry, low on time... code not readable
        let rawData = 
            csvData |> List.map (fun row -> 
            row |> List.mapi (fun i item -> getItemOrDefault (item, i) )) // Drop "index" if not used...should CSV reader be aware??
            |> List.map (fun row -> 
            row |> List.toArray)
            |> List.toArray

        let standardizer  = Standardizer(rawData, outPutIndex, collumnTypes |> List.toArray)
        let standardisedData = standardizer.StandardizeAll (rawData)
        let dataToReturn = Array.toList(standardisedData) |> List.map (fun row -> Array.toList row)
        
        (dataToReturn, standardizer)
                    

// If requiredOutputIndex is provided, columns after this index represents output, alternatively assumes las column contains desired output       
let getTrainingDataAndDesiredOutput (epoc, inputDataIndex) = 
    
    let inputs = epoc |> List.map (fun row -> 
            row |> Seq.take (inputDataIndex) 
            |> Seq.toList)

    let desiredOutputs = epoc |> List.map (fun row -> 
            row |> Seq.skip (inputDataIndex) 
            |> Seq.toList)
   
    (inputs, desiredOutputs)
    

let getFirstHalf (list : List<'T>) = 
    list |> Seq.take (list.Length / 2) |> Seq.toList


let getSecondHalf ( list : List<'T>) = 
    list |> Seq.skip (list.Length / 2) |> Seq.toList

let printActualAndDesiredOutputs actual desired =

    let printRow output = 
        printf "("
        output |> Seq.iter (fun x -> printf "%f;" (x))
        printf ")"

    List.map2 (fun actualRow desiredRow -> (
            printRow actualRow
            printf "|"
            printRow desiredRow
            printf "|\n"
            )
        ) actual desired
        |> ignore
    
