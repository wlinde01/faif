## Synopsis

FAIF is a functional (**as in the programming paradigm**) Neural Network with backpropagation built with F#. 
Note that I am no F# guru and have basically learned F# for the purpose of building this project so I am sure there are many many potential enhancements and optimisations. 
Regardless, it was attempted to use as far as possible idiomatic F# and a focus was placed on good functional practices. In here you will find lots of list and associated function mappings, recursion, hopefully mainly tail recursion and only ever immutable data structures. 
 



## Motivation

FAIF starts life as an MSc. project to implement a foundational starting point for an extendable Neural Network in F#. The hope is that it might be useful for others learning F# or those learning to come to grips with the (or an)  architecture that drives a Neural Network implementation. 


[Birkbeck Institute for Data
Analytics](http://www.bida.bbk.ac.uk "Birkbeck Institute for Data
Analytics")

[Birkbeck Computer Science Department](https://www.dcs.bbk.ac.uk/ "Birkbeck Computer Science Department").



## Installation

FAIF currently exists as source code only. Please feel free to contribute and provide wrapper executables around working examples. To make use of it, please download Visual Studio and clone this repo. Various FSUnit and XUnit tests are provided to make FAIF extensibility safer and to provide a form of documentation to any potential contributors. 


## Code Examples


### Create a Perceptron that learns to produce the logical AND behaviour
    let inputs = [[0.0;0.0];[0.0;1.0];[1.0;0.0];[1.0;1.0]]
    let desiredOutputs = [0.0;0.0;0.0;1.0]
    let initialWeights = [0.3; -0.1]
    let trainedPerceptron =  trainPerceptronWithEpocUntil(inputs, desiredOutputs, initialWeights, 3, threshold, learningRate, StepFunction)

    // Perceptron should be trained by now, test it's outputs / activations
    trainedPerceptron.Activate([0.0;0.0]), StepFunction)    // Produces 0.0
    trainedPerceptron.Activate([0.0;1.0], StepFunction)     // Produces 0.0
    trainedPerceptron.Activate([1.0;0.0], StepFunction)     // Produces 0.0
    trainedPerceptron.Activate([1.0;1.0], StepFunction)     // Produces 1.0

### Create a NN that learns to produce the logical XOR behaviour
    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = { 
            Neurons = [ Neuron.create [0.5; 0.4]  0.8;
                        Neuron.create [0.9; 1.0] -0.1]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = HiddenLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let outputLayer = { 
            Neurons = [Neuron.create [-1.2; 1.1] 0.3]; 
            ActivationFunction = HyperbollicTangentSigmoidFunction;
            GradientDescentFunction = OutputLayerHyperbollicTangentGradientFunction;
            WeightTrainingFunction = adjustWeightsSimple;
        }

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 1.85, xorNetwork, 0.001)

    
    let result = trainedXORNetwork.Activate inputs.[0] // produces [0.0]    
    


### Create a two layer logistic sigmoid based network with momentum and teach it  to produces XOR results correctly from random starting point weights

    let inputs = [[1.0;1.0];[0.0;1.0];[1.0;0.0];[0.0;0.0]]
    let desiredOutputs = [[0.0];[1.0];[1.0];[0.0]]

    let hiddenLayer = Layer.Spawn 2 2 LogisticSigmoidFunction HiddenLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 
    let outputLayer = Layer.Spawn 1 2 LogisticSigmoidFunction OutputLayerLogisticErrorGradientFunction adjustWeightsWithMomentum 

    let  xorNetwork = { Layers = [hiddenLayer; outputLayer] }

    let trainedXORNetwork = trainNetworkWithEpocUntilErrorIswithinMargin (inputs, desiredOutputs, 0.1, xorNetwork, 0.001) // Produces "good results in low ish amount of iterations"

    let index = 0
    let result = trainedXORNetwork.Activate inputs.[index]
    // result.[0] is equal to [0.0]
    
    let index = 1
    let result = trainedXORNetwork.Activate inputs.[index]
    // result.[0]  is equal to [1.0]

    let index = 2
    let result = trainedXORNetwork.Activate inputs.[index]
    // result.[0]  is equal to [1.0]

    let index = 3
    let result = trainedXORNetwork.Activate inputs.[index]
    // result.[0]  is equal to [0.0]

### Open a CSV file with training data - assumes no normilisation is required (file provided with repo for reference) 
    let csvReader = CsvReader("C:\\location\\off\\faif\\FAIF\\FAIF\\trainingdata")
    // CSV File convention:
    // First line contains headings
    // Second line contains default values (if data is missing or value=?, this default is used instead)
    let csvData : List<List<float>> = csvReader.openCsv ("xor") 
    
    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 2) // splits intputs from desired outputs

### Open a CSV file with training data and normalises the date (file provided with repo for reference)
    let csvReader = CsvReaderWithNormilization("C:\\location\\off\\faif\\FAIF\\FAIF\\trainingdata")
    // CSV File convention:
    // First line contains headings
    // Second line contains collumn data types (numeric|categorical)
    // Third line contains default values (if data is missing or value=?, this default is used instead)
    let csvData, normalizer = csvReader.openCsv (19,"cervicalcancer-citology") 

    let inputs, desiredOutputs = getTrainingDataAndDesiredOutput (csvData, 19)

### Network saving and retrieving
    // Comes in two flavours, saving a network with it's normilizor (shown) and a version for persisting network only.
    let persister = Persiter() // when no folder provided, defaults to current working directory
    do persister.saveNormalizedNetwork (networkWithNormalizer, "networkName") |> ignore
    let retrievedNetwork : NormalizedNetwork =  persister.openNormalizedNetwork ("networkName")

### Show a JSON representation of the network state (excludes Activation and Weight training functions)
    // Comes in two flavours, serialisation of a network with it's normilizor (shown) and a version for serialising network only.
    printf "\nNetwork:\n%s" (SerializeToJson2(retrievedNetwork))

### There is more, browse the tests in there to see it in action. 

## Tests
### Note that, at time of writing, certain tests have hardcoded file and folder names. Please keep this in mind when trying to run these tests. There is a convention for file and folder locations, generally removing any folder names will result in the file saving and opening to proceed from the current working directory.

### Please also note that, at time of writing the Cervical Cancer Tests are not adequate due to various reasons. Little to now convergence currently happens and when the error signal does become small enough the NN will usually only correctly a assess a small handful of positive diagnosis as the data set is too small to be statistically significant.

The FAIF solution contains a test project named : NeuralNetwork.Tests. 
Tests are produced with xunit and FSunit and at the time of creation was run via NCrunch but the built in VS test runner should suffice. Please be aware that some of the tests start from randomly initialised weights and hence there is no guarantee that the network will converge. There is also no automated stopping, so unless timeouts are set for test runs, tests very well may run indefinitely. 

## Contributors

Please feel free to jump in and play with it or contribute. Questions welcome and I am sure some work will be required to make it easily runnable on contributors setups. Again, any and all questions and suggestions welcome. 




## License

Free, as in beer, that is free. 